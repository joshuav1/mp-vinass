<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");

include '../../essentials/connection.php';

$conn = new Connection();

if(!$conn->connect()) die('Configuration error');
//else echo 'successful connection'; //must delete this upon finalization

$to_decode = json_decode(file_get_contents("php://input"));

$sql = "SELECT ID, username, name, address, contact FROM user_tbl";
$result = mysqli_query($conn->connect(), $sql);
$resultcheck = mysqli_num_rows($result);

if ($resultcheck > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        echo json_encode(array('ID' =>  $row["ID"] , 'username' =>  $row["username"], 'name' =>  $row["name"], 'address' =>  $row["address"], 'contact' =>  $row["contact"]));
    }
} else {
    http_response_code(403);
    echo json_encode(array('msg' => 'username not found'));
}




$conn->close($conn->connect());
?>